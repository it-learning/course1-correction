#!/usr/bin/env bash

####################################################################################################
# 0. Setup

# Constants
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# Load the configuration
source ${SCRIPTPATH}/config.env

# Compute the destination folder with configuration
folder_name="${DESTINATION}/course"

####################################################################################################
# 1. Update the course repository from overlead (login required)
$SCRIPTPATH/update_course.sh 1&> /dev/null


# 2. Generate the PDF file
( cd ${folder_name}; make release 1&> /dev/null ) && pdf_name=$(ls ${folder_name}/*.pdf)

# 3. Print the path of the generated PDF file
echo "PDF Course available at: ${folder_name}/${pdf_name}"
