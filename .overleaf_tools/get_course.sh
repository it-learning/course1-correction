#!/usr/bin/env bash

####################################################################################################
# 0. Setup

# Constants
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# Load the configuration
source ${SCRIPTPATH}/config.env

# Compute the destination folder with configuration
folder_name="${DESTINATION}/course"

####################################################################################################
# 1. Clone the course repository from overlead (login required)
( cd ${SCIPT_PATH}; git clone https://git.overleaf.com/${OVERRLEAF_GIT_HASH} ${folder_name} )
