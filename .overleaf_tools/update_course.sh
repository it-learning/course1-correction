#!/usr/bin/env bash

####################################################################################################
# 0. Setup

# Constants
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# Load the configuration
source ${SCRIPTPATH}/config.env

# Compute the destination folder with configuration
folder_name="${DESTINATION}/course"

####################################################################################################
# 1. update the course repository from overlead (login required)
( cd ${folder_name}; git pull -r origin HEAD)
