#!/usr/bin/env python

import random
import string
import copy
# TODO: import time and mesure tested / checker function time (comparaison, average, etc...)

class testObject():
    """
        Test class used to bind the functionnal procedures to tests
    """
    # Public attributes
    is_verbose = True

    # Private global attributes
    _test_nb = 0

    def _get_and_inc_test_nb(self):
        """
            Get the current test_nb, then update random seed with increment,
            to always have the same random sequence.
            @return   test_nb   The number of current test (before increment)
        """
        self._test_nb += 1

        return self._test_nb - 1


    def add_function_to_test(self, function, args, expected_result_func, random_size=None):
        """
            Add a function to test
            @param   function      The function under test.
            @param   args          The argument of the given function, if 'int', 'str' or 'bool' is
                                   given, random object are generated.
            @param   expected_result_func   The expected result (over a function process
                                            with same args)
            @param   random_size            Expected size for random generation: 'low', 'medium',
                                            'high' or integer.
        """
        # Size if used to generate argument at desired proportion
        if random_size == 'low':
            self.size = 4
        elif random_size == 'medium':
            self.size = 42
        elif random_size == 'high':
            self.size = 4242
        else:
            self.size = int(random_size)

        self._func_array.append((function, args, expected_result_func))


    def __init__(self, function=None, args=None, expected_result_func=None, random_size=None):
        """
            Constructor of the test object
            @param   function               The function under test.
            @param   args                   The argument of the given function, if 'int', 'str'
                                            or 'bool' is given, random object are generated.
            @param   expected_result_func   The expected result (over a function process
                                            with same args)
            @param   random_size            Expected size for random generation: 'low', 'medium',
                                            'high' or integer.
        """
        # Private local attributes
        self._func_array = []

        if function is not None:
            self.add_function_to_test(function, args, expected_result_func, random_size)

    def run_test(self, index=0):
        """
            Run the given test (or the first one if nothing specified)
            @param   index    The index of the test to run (default 0)
            @return  result   True if test is PASSED, False if test is FAILED
        """
        # Initialize the random generator (with test_nb to always have the same random sequence
        random.seed(self._test_nb);

        func, args, expect = self._func_array[index]

        # Generate arg if needed (but do not update them)
        args = copy.deepcopy(args)
        for i, arg in enumerate(args):
            if arg == int:
                arg = random.randint(0, self.size)
            elif arg == str:
                arg = ''.join(random.choice(string.ascii_lowercase) for _ in range(self.size))
            elif arg == bool:
                arg = True if random.randint(0, 1) == 1 else False
            args[i] = arg

        if args is not None:
            output = func(*args)
            reference = expect(*args)
        else:
            output = func()
            reference = expect()

        result = (output == reference)

        if self.is_verbose:
            func_str = func.__name__
            ref_str = str(reference) + ("" if result else " [K0]")
            test_nb = self._get_and_inc_test_nb()
            print("[%s] %s(%s) -> %s - expected:%s" % (test_nb, func_str, args, output, ref_str))

        return result

    def run_tests(self):
        """
            Run all the register (added) tests
            @return  result   True if tests are PASSED, False if tests are FAILED
        """
        results = True

        for index in range(len(self._func_array)):
            results = results and self.run_test(index)

        return results

class dummyChecker():
    """
        Dummy Checker object used to process inputs/outputs value over a given test object
    """

    def execute(self, test_object, nb_times=1):
        """
            Execute the test collection from the given test object and return a boolean value,
            indicating if the run of test(s) is PASSED or FAILED
            @param   testObject     Test object under test (an error if print if object is not
                                    the expected one
            @param   nb_times       The number of time test object shall be played (default 1)

            @return  glob_results   True if test(s) PASSED, False if test(s) FAILED
        """
        results = False

        if type(test_object) != type(testObject()):
            print("Cannot run the test(s), bad execute() argument type")
        else:
            glob_result = True
            for index in range(nb_times):
                glob_result = test_object.run_tests() and glob_result

        if glob_result:
            print("Test(s) PASSED !")
        else:
            print("Test(s) FAILED /!\\")

        return glob_result
