#!/usr/bin/env python

import dummyChecker
import math

####################################################################################################
# Correction of Euclide PGCD
####################################################################################################

def pgcd(a, b):
    """
        The Euclide PGCD algorithm
        @param    a        fisrt number for common denominator
        @param    b        second number for common denominator

        @return   result   The most common denominator
    """
    assert(a > 0 and b > 0)  # P: Prédicat de départ

    while (a > 0 and b > 0):
        if a > b:
            assert(math.gcd(a, b) == math.gcd(a - b, b))  # I.1 check solution when a contain at most b
            a = a - b
        else:
            assert(math.gcd(a, b) == math.gcd(a, b - a))  # I.2 check solution when b contain at most a
            b = b - a

    result = max(a, b)
    assert(result == math.gcd(a, b))  # Q: solution attendue

    return result


def main():
    """
        The main program
    """
    test = dummyChecker.testObject(function=pgcd,
                                   args=[int, int],
                                   expected_result_func=math.gcd,
                                   random_size='high')
    dummyChecker.dummyChecker().execute(test, 10)

if __name__ == '__main__':
    main()
