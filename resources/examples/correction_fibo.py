#!/usr/bin/env python

import dummyChecker

####################################################################################################
# Correction of fibonacci
####################################################################################################

def fibo_comparator(n):
    """
        The fibonacci sequence, code take from:
        https://waytolearnx.com/2019/04/programme-python-pour-afficher-la-suite-de-fibonacci.html
        @param    n        Number of fibonacci iteration

        @return   result   The result of the fibonacci algorithm

    """
    if(n <= 1):
        return n
    else:
        return (fibo_comparator(n-1) + fibo_comparator(n-2))


def fibo(n):
    """
        The fibonacci sequence:
        @param    n        Number of fibonacci iteration

        @return   result   The result of the fibonacci algorithm
    """
    assert(n >= 0)  # P : Prédicat de départ
    x = 0
    assert(x == fibo_comparator(0)) # I.1 check init case fibo(0)
    y = 1
    assert(y == fibo_comparator(1)) # I.2 check init case fibo(0)
    for k in range(2, n + 1):
        assert(0 < k and k < n + 1)  # I.3 check the loop condition
        assert(x == fibo_comparator(k-2) and y == fibo_comparator(k-1))  # I.4 check current fibo computation
        z = x + y
        assert(z == fibo_comparator(k))  # I.5 check 2nd previous fibo computation is saved
        x = y
        assert(x == fibo_comparator(k-1))  # I.6 check last fibo computation is saved
        y = z
        assert(y == fibo_comparator(k))  # I.7 check new fibo computation

    assert(y == fibo_comparator(n))  # Q: solution attendue
    return y


def main():
    """
        The main program
    """
    test = dummyChecker.testObject(function=fibo,
                                   args=[int],
                                   expected_result_func=fibo_comparator,
                                   random_size=20)
    dummyChecker.dummyChecker().execute(test, 25)

if __name__ == '__main__':
    main()
